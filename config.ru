$LOAD_PATH << File.join(__dir__, "lib")

require 'json'

require ENV.fetch('FUNCTION_PATH')
bot = Kernel.const_get(ENV.fetch('FUNCTION_NAME')).new

app = lambda do |env|
  req = Rack::Request.new(env)
  payload = JSON.parse(req.body.read)
  bot.call(payload)
  [200, {}, StringIO.new('success')]
end

webhook_secret = ENV.fetch('WEBHOOK_SECRET')
authenticated_app = Rack::Auth::Basic.new(app) do |username, password|
  Rack::Utils.secure_compare(webhook_secret , password)
end

run authenticated_app
