require 'rest-client'

class ThroughputLabels
  TEAM_LABELS = ["Configure"]
  THROUGHPUT_LABELS = [
    "bug",
    "feature",
    "security",
    "Community contribution",
    "backstage",
  ]
  HINT_MESSAGE = "Please add one of the following throughput labels: #{THROUGHPUT_LABELS.map { |l| "~\"#{l}\""}.join(", ")}"
  GITLAB_BASE_URL = ENV.fetch('GITLAB_BASE_URL')
  API_TOKEN = ENV.fetch('API_TOKEN')

  def call(payload)
    project_id = payload.fetch("project").fetch("id")
    merge_request_iid = payload.fetch("object_attributes").fetch("iid")

    labels = payload.fetch("labels").map { |l| l.fetch("title") }

    return if (labels & TEAM_LABELS).empty?
    return unless (labels & THROUGHPUT_LABELS).empty?
    return unless payload.fetch("object_attributes").fetch("action") == "open"

    url = "#{GITLAB_BASE_URL}/api/v4/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions"
    puts "Notifying on #{url}"
    RestClient.post(
      url,
      {body: HINT_MESSAGE}.to_json,
      {content_type: :json, accept: :json, "PRIVATE-TOKEN": API_TOKEN}
    )
  end
end
