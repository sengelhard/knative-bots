require 'spec_helper'
ENV['API_TOKEN'] = 'the-token'
ENV['GITLAB_BASE_URL'] = 'https://mygitlab.example.com'
require 'throughput_labels'

describe ThroughputLabels do
  describe '#call' do
    context 'when the merge request contains Configure team and no throughput label' do
      let(:payload) do
        {
          "labels" => [
            { "title" => "Configure" },
          ],
          "changes" => { "created_by_id" => 123 },
          "project" => { "id" => 987 },
          "object_attributes" => { "iid" => 456, "action" => "open" }
        }
      end

      it 'posts a discussion item asking to add throughput labels' do
        stub = stub_request(:post, "https://mygitlab.example.com/api/v4/projects/987/merge_requests/456/discussions")
          .with(
            body: {body: 'Please add one of the following throughput labels: ~"bug", ~"feature", ~"security", ~"Community contribution", ~"backstage"'},
            headers: {"PRIVATE-TOKEN" => 'the-token', "Content-Type" => "application/json"}
        )

        ThroughputLabels.new.call(payload)

        expect(stub).to have_been_requested
      end
    end

    context 'when the payload contains no team label' do
      let(:payload) do
        {
          "labels" => [
          ],
          "changes" => { "created_by_id" => 123 },
          "project" => { "id" => 987 },
          "object_attributes" => { "iid" => 456, "action" => "open" }
        }
      end

      it 'does not make a request' do
        ThroughputLabels.new.call(payload)
      end
    end

    context 'when the payload contains a throughput label' do
      let(:payload) do
        {
          "labels" => [
            { "title" => "Configure" },
            { "title" => "bug" },
          ],
          "changes" => { "created_by_id" => 123 },
          "project" => { "id" => 987 },
          "object_attributes" => { "iid" => 456, "action" => "open" }
        }
      end

      it 'does not make a request' do
        ThroughputLabels.new.call(payload)
      end
    end

    context 'when the event is not an open merge request action' do
      let(:payload) do
        {
          "labels" => [
            { "title" => "Configure" },
          ],
          "changes" => { "created_by_id" => 123 },
          "project" => { "id" => 987 },
          "object_attributes" => { "iid" => 456, "action" => "update" }
        }
      end

      it 'does not make a request' do
        ThroughputLabels.new.call(payload)
      end
    end
  end
end
